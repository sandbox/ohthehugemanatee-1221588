$Id


Description
-----------

A common problem with webforms in Drupal 6 is that you can't use data from other nodes to fill fields.  
This module helps to get around that restriction.  If you can get the NID of the data source into a 
field on the form, this module will pull arbitrary data from a field of your choice on that NID, and
put it into a field on the webform.  

A common use case is email addresses; if you have a content type where each node has its own email 
address, you can use this module to build a Webform block which automatically fills a hidden "email 
destination" field on the webform with the email address from the node.

Prerequisites
-------------
Webform module - this doesn't make any sense without it.

Installation
------------
Simply copy this module to your modules directory, enable it, and configure it in 
admin/settings/webform-node-data .

