<?php
// $Id$

/**
 * @file
 * Administration page callbacks for the webform_node_data module.
 */

/**
 * Implementation of hook_admin_settings().
 * 
 * @ingroup forms
 * @see system_settings_form().
 */
function webform_node_data_admin_settings() {
  // Description of how the module works

  // Webform ID to apply to
  $form['webform_node_data_webform_nid'] = array(
    '#type' => 'textfield',
    '#title' => t('Webform NID'),
    '#description' => t('Enter the NID of the webform which will have a filled element.  Enter 0 to apply to all webforms.'),
    '#size' => 5,
    '#maxlength' => 5,
    '#required' => TRUE,
    '#default_value' => variable_get('webform_node_data_webform_nid', '0') 
  );

  // Webform field which contains the NID
  $form['webform_node_data_source_nid_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Source NID Field'),
    '#description' => t('Webform field ID which contains the Source NID to pull target data from. Enter <active> if the webform is a block, to automatically get the NID of the node being displayed.'),
    '#size' => 30,
    '#required' => TRUE,
    '#default_value' => variable_get('webform_node_data_source_nid_field', '<active>')
  );

  // Name of the field (on the node) to copy FROM
  $form['webform_node_data_source_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Target Data Field'),
    '#description' => t('Field ID on the Source Node which contains the Target Data'),
    '#size' => 30,
    '#required' => TRUE,
    '#default_value' => variable_get('webform_node_data_source_field', 'field_email')
  );

  // Type of data in the source field
  $form['webform_node_data_source_datatype'] = array(
    '#type' => 'textfield',
    '#title' => t('Source field type'),
    '#description' => t('CCK Field type for the source field.  textarea, email, etc'),
    '#size' => 12,
    '#required' => TRUE,
    '#default_value' => variable_get('webform_node_data_source_datatype', 'email')
  );

  // Webform field to copy node field data TO
  $form['webform_node_data_destination_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Destination Field'),
    '#description' => t('Webform field which should receive the Target Data'),
    '#size' => 30,
    '#required' => TRUE,
    '#default_value' => variable_get('webform_node_data_destination_field', 'destination_email')
  );

  return system_settings_form($form);

}

/**
 * Validate the configuration form
 */
function webform_node_data_admin_settings_validate($form, $form_state) {
  $nid = $form_state['values']['webform_node_data_webform_nid'];
  if (!is_numeric($nid)) {
    form_set_error('webform_node_data_webform_nid', t('Please enter a numeric webform NID'));
  }
}
